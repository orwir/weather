package ingvar.weather.utils;

import android.net.Uri;
import android.os.AsyncTask;

import java.io.IOException;

import ingvar.weather.utils.handlers.HttpHandler;

public class WeatherReceiver extends AsyncTask<Uri, Void, HttpResponse> {

    private HttpHandler handler;

    public WeatherReceiver(HttpHandler handler) {
        this.handler = handler;
    }

    @Override
    protected HttpResponse doInBackground(Uri... urls) {
        HttpResponse result = null;
        try {
            result = NetworkHelper.downloadUrl(urls[0]);
        } catch (IOException e) {
            result = new HttpResponse(404, "Unable to retrieve web page. URL may be invalid.");
        }
        return result;
    }

    @Override
    protected void onPostExecute(HttpResponse result) {
        if(handler != null) {
            handler.handle(result);
        }
    }

}
