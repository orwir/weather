package ingvar.weather.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Date;

import ingvar.weather.dto.Weather;
import ingvar.weather.dto.WeatherType;

public class SharedPreferencesCache {

    private static final String SHARED_PREFERENCE_NAME = "cache";

    //TODO: remove after create Contract class
    private static final String WEATHER_CITY = "city";
    private static final String WEATHER_TYPE = "type";
    private static final String WEATHER_TEMPERATURE = "temperature";
    private static final String WEATHER_ACTUALITY = "actuality";

    /**
     * Return cached weather info. If date expired or city does not match return null
     * @param context
     * @param city
     * @return
     */
    public static Weather load(Context context, String city) {
        Weather weather = null;

        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);

        String cachedCity = prefs.getString(WEATHER_CITY, null);
        if(cachedCity != null && cachedCity.equals(city)) {
            long currentTime = System.currentTimeMillis();
            long actuality = prefs.getLong(WEATHER_ACTUALITY, 0);
            if(actuality + StorageHelper.EXPIRATION_TIME > currentTime) {
                weather = new Weather();
                weather.setCity(city);
                weather.setType(WeatherType.values()[prefs.getInt(WEATHER_TYPE, 0)]);
                weather.setTemperature(prefs.getInt(WEATHER_TEMPERATURE, 0));
                weather.setActuality(new Date(actuality));
            }
        }

        return weather;
    }

    public static void save(Context context, Weather weather) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(WEATHER_CITY, weather.getCity());
        editor.putInt(WEATHER_TYPE, weather.getType().ordinal());
        editor.putInt(WEATHER_TEMPERATURE, weather.getTemperature());
        editor.putLong(WEATHER_ACTUALITY, weather.getActuality().getTime());

        editor.commit();
    }

    public static void delete(Context context, String city) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        String cachedCity = prefs.getString(WEATHER_CITY, null);
        if(cachedCity != null && cachedCity.equals(city)) {
            editor.putString(WEATHER_CITY, null);
            editor.putString(WEATHER_TYPE, null);
            editor.putString(WEATHER_TEMPERATURE, null);
            editor.putString(WEATHER_ACTUALITY, null);

            editor.commit();
        }
    }

    private SharedPreferencesCache() {}

}
