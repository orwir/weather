package ingvar.weather.utils;

import android.content.Context;

import ingvar.weather.dto.Weather;
import ingvar.weather.dto.WeatherWrapper;
import ingvar.weather.utils.forecast.OpenWeatherMapCom;
import ingvar.weather.utils.handlers.HttpHandler;
import ingvar.weather.utils.handlers.WeatherHandler;

public class StorageHelper {

    public static final long EXPIRATION_TIME = 1000 * 60 * 60; //1 hour

    public static void updateWeather(final Context context, String city, final WeatherHandler handler) {
        Weather weather = SharedPreferencesCache.load(context, city);
        if(weather == null && city != null) {
            HttpHandler httpHandler = new HttpHandler() {
                @Override
                public void handle(HttpResponse response) {
                    if (200 == response.getCode()) {
                        WeatherWrapper wrapper = OpenWeatherMapCom.parseJson(response.getText());
                        handler.handle(wrapper);
                        if(WeatherWrapper.Result.OK.equals(wrapper.getResult())) {
                            cacheWeather(context, wrapper.getWeather());
                        }
                    } else {
                        WeatherWrapper wrapper = new WeatherWrapper(null, WeatherWrapper.Result.ERROR, response.getText());
                        handler.handle(wrapper);
                    }
                }
            };
            if (NetworkHelper.isConnected(context)) {
                new WeatherReceiver(httpHandler).execute(OpenWeatherMapCom.createUri(city));
            } else {
                handler.handle(new WeatherWrapper(null, WeatherWrapper.Result.ERROR, "No internet connection"));
            }
        }
        else if(weather != null) {
            handler.handle(new WeatherWrapper(weather, WeatherWrapper.Result.OK, null));
        }
    }

    public static void cacheWeather(Context context, Weather weather) {
        SharedPreferencesCache.save(context, weather);
    }

    public static void clearCache(Context context, String city) {
        SharedPreferencesCache.delete(context, city);
    }

    private StorageHelper() {}

}
