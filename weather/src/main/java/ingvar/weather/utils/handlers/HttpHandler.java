package ingvar.weather.utils.handlers;

import ingvar.weather.utils.HttpResponse;

public interface HttpHandler {

    void handle(HttpResponse response);

}
