package ingvar.weather.utils.handlers;

import ingvar.weather.dto.WeatherWrapper;

public interface WeatherHandler {

    void handle(WeatherWrapper wrapper);

}
