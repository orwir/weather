package ingvar.weather.utils.forecast;

import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import ingvar.weather.dto.Weather;
import ingvar.weather.dto.WeatherType;
import ingvar.weather.dto.WeatherWrapper;

public class OpenWeatherMapCom {

    private static final String URI = "http://api.openweathermap.org/data/2.5/weather";
    private static final String PARAM_CITY = "q";
    private static final String PARAM_MODE = "mode"; //xml or json
    private static final String PARAM_UNITS = "units"; //internal, metric, or imperial

    private static final String JSON_CODE = "cod";
    private static final String JSON_ERROR = "message";
    private static final String JSON_CITY = "name";
    private static final String JSON_MAIN = "main";
    private static final String JSON_TEMP = "temp";
    private static final String JSON_WEATHER = "weather";
    private static final String JSON_WEATHER_ID = "id";

    public static Uri createUri(String city) {
        Uri.Builder builder = new Uri.Builder();
        builder.encodedPath(URI);
        builder.appendQueryParameter(PARAM_CITY, city);
        builder.appendQueryParameter(PARAM_UNITS, "metric");

        return builder.build();
    }

    public static WeatherWrapper parseJson(String json) {
        WeatherWrapper result = new WeatherWrapper();
        try {
            JSONObject answer = new JSONObject(json);
            int code = Integer.valueOf(answer.getString(JSON_CODE));
            if(200 == code) {
                Weather weather = new Weather();
                weather.setCity(answer.getString(JSON_CITY));
                weather.setActuality(new Date());
                weather.setTemperature(answer.getJSONObject(JSON_MAIN).getInt(JSON_TEMP));
                weather.setType(getWeatherType(answer.getJSONArray(JSON_WEATHER).getJSONObject(0).getString(JSON_WEATHER_ID)));

                result.setWeather(weather);
                result.setResult(WeatherWrapper.Result.OK);
            } else {
                result.setResult(WeatherWrapper.Result.ERROR);
                result.setErrorMessage(answer.getString(JSON_ERROR));
            }
        } catch(JSONException e) {
            result.setResult(WeatherWrapper.Result.ERROR);
            result.setErrorMessage(e.getMessage());
        }
        return result;
    }

    private static WeatherType getWeatherType(String type) {
        WeatherType result = WeatherType.UNKNOWN;
        switch (Integer.valueOf(Character.toString(type.charAt(0)))) {
            case 2: //Thunderstorm
                result = WeatherType.STORM;
                break;
            case 3: //Drizzle
            case 5: //Rain
                result = WeatherType.RAINY;
                break;
            case 6: //Snow
                result = WeatherType.SNOW;
                break;
            case 8: //Clouds
                result = "800".equals(type) ? WeatherType.FAIR : WeatherType.CLOUDY;
                break;
        }
        return result;
    }

}
