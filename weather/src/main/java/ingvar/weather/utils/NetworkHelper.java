package ingvar.weather.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetworkHelper {

    public static boolean isConnected(Context context) {
        ConnectivityManager mgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = mgr.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected();
    }

    public static HttpResponse downloadUrl(Uri uri) throws IOException {
        HttpResponse result = null;

        InputStream is = null;
        HttpURLConnection conn = null;
        try {
            URL url = new URL(uri.toString());
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            is = conn.getInputStream();

            // Convert the InputStream into a string
            Reader reader = new InputStreamReader(is, "UTF-8");
            StringBuilder str = new StringBuilder();
            int c;
            while((c = reader.read()) != -1) {
                str.append((char)c);
            }
            result = new HttpResponse(conn.getResponseCode(), str.toString());

            reader.close();
        } finally {
            if(conn != null) {
                conn.disconnect();
            }
            if (is != null) {
                is.close();
            }
        }

        return result;
    }

    private NetworkHelper() {}

}
