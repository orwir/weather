package ingvar.weather.views;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import ingvar.weather.R;
import ingvar.weather.dto.Weather;
import ingvar.weather.dto.WeatherWrapper;
import ingvar.weather.utils.StorageHelper;
import ingvar.weather.utils.handlers.WeatherHandler;


public class WeatherActivity extends ActionBarActivity {

    private ImageView type;
    private TextView temperature;
    private TextView city;

    private String cityName;

    public void forceRefresh(MenuItem item) {
        StorageHelper.clearCache(this, cityName);
        refresh();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        type = (ImageView) findViewById(R.id.weather_icon);
        temperature = (TextView) findViewById(R.id.weather_tmp);
        city = (TextView) findViewById(R.id.weather_city);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            cityName = intent.getStringExtra(SearchManager.QUERY);
        }
    }

    private void refresh() {
        if(cityName != null && !cityName.isEmpty()) {
            StorageHelper.updateWeather(this, cityName, new WeatherHandler() {
                @Override
                public void handle(WeatherWrapper wrapper) {
                    if (WeatherWrapper.Result.OK.equals(wrapper.getResult())) {
                        Weather weather = wrapper.getWeather();
                        type.setBackgroundResource(weather.getType().getImage());
                        temperature.setText(getResources().getString(R.string.weather_tmp, weather.getTemperature()));
                        city.setText(weather.getCity());
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(WeatherActivity.this);
                        builder.setTitle(R.string.warining);
                        builder.setMessage(wrapper.getErrorMessage());
                        builder.setPositiveButton(R.string.ok, null);
                        builder.create().show();
                    }
                }
            });
        }
    }

}
