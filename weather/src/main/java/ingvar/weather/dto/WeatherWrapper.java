package ingvar.weather.dto;

public class WeatherWrapper {

    public static enum Result {
        OK,
        ERROR;
    }

    private Weather weather;
    private Result result;
    private String errorMessage;

    public WeatherWrapper() {}

    public WeatherWrapper(Weather weather, Result result, String errorMessage) {
        this.weather = weather;
        this.result = result;
        this.errorMessage = errorMessage;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
