package ingvar.weather.dto;

import ingvar.weather.R;

public enum WeatherType {

    UNKNOWN(R.drawable.weather_unknown),
    FAIR(R.drawable.weather_fair),
    CLOUDY(R.drawable.weather_cloudy),
    RAINY(R.drawable.weather_rainy),
    STORM(R.drawable.weather_storm),
    SNOW(R.drawable.weather_snow);

    private final int image;

    WeatherType(int image) {
        this.image = image;
    }

    public int getImage() {
        return image;
    }

}
