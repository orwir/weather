package ingvar.weather.dto;

import java.util.Date;

public class Weather {

    private String city;
    private WeatherType type;
    private int temperature;
    private Date actuality;

    public Weather() {}

    public Weather(String city, WeatherType type, int temperature, Date actuality) {
        this.city = city;
        this.type = type;
        this.temperature = temperature;
        this.actuality = actuality;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public WeatherType getType() {
        return type;
    }

    public void setType(WeatherType type) {
        this.type = type;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public Date getActuality() {
        return actuality;
    }

    public void setActuality(Date actuality) {
        this.actuality = actuality;
    }

}
